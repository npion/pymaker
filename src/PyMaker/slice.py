# Daniel Seabra de Andrade
# PyMaker - Pygame Wrapper for Game Maker Users

import pygame


class Slice(pygame.sprite.Sprite):
    """An object for Game Maker users. In particular, it implements some of
    the standard variables that Game Maker objects have, among them:
        x: The x-coordinate of the instance
        y: The y-coordinate of the instance
        image: This instance's image (takes a string for loading, but calling
               self.image will return an image object)
        solid: Whether this object will check for collisions
        depth: Integer depth at which to draw this object (objects with lower
               depths will appear above objects with larger depths)
        
        Extend this object to use it by using this syntax:
            class Name(PyMaker.slice.Slice)
    """
    lst = []  # List of objects, class variable

    def __init__(self, x=0, y=0, image=None, solid=False, depth=0):
        """
        Constructor
        """
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(image)
        (self.x, self.y, self.depth) = (x, y, depth)
        self.solid = solid
        
        if solid:
            self.rect = pygame.Rect(self.image.get_rect())
            self.rect.center = (x, y)
            (self.w, self.h) = (self.rect.width, self.rect.height)
        
        self.setConstruct()
        self.onCreate()

    def setConstruct(self):
        """Add this object to the list of objects intelligently while
        maintaining proper order by depth
        """
        if not Slice.lst:
            Slice.lst.append(self)
        else:
            lowerBound = 0
            upperBound = len(Slice.lst)
            
            while True:
                pos = (lowerBound + upperBound) / 2
                depth = Slice.lst[pos].depth
                
                if pos == upperBound or pos == lowerBound:
                    if self.depth > depth:
                        Slice.lst.insert(lowerBound + 1, self)
                    else:
                        Slice.lst.insert(lowerBound, self)
                    break
                
                if depth > self.depth:
                    upperBound = pos
                elif depth < self.depth:
                    lowerBound = pos
                else:
                    Slice.lst.insert(pos, self)
                    break
    
    def instanceDestroy(self):
        """Destroys this instance (it will no longer have any of its methods
        called)
        """
        self.onDestroy()
        Slice.lst.remove(self)
        del self
    
    def onCreate(self):
        pass
    
    def onStep(self):
        pass
    
    def onBeginStep(self):
        pass
    
    def onEndStep(self):
        pass
    
    def onPreDraw(self):
        pass
    
    def onDraw(self):
        pass

    def onPostDraw(self):
        pass
    
    def onDestroy(self):
        pass
