'''
Created on Aug 3, 2013

@author: Owner
'''

import sys
import pygame
from pygame.locals import K_UP, K_DOWN, K_LEFT, K_RIGHT, K_DELETE, \
                          K_ESCAPE, DOUBLEBUF, QUIT
from PyMaker.slice import Slice

# Constants
WINDOW_SIZE = (640, 480)
SLEEP_TIME = 60

window = pygame.display.set_mode(WINDOW_SIZE, DOUBLEBUF)


class Example(Slice):
    """An example class. Inherits Slice. You can pass all sort of things to
    this object and you can also implement default arguments. For example, we
    are passing 'bogos.gif' to the Slice as a mandatory image, so Examples can
    only have 'bogos.gif' as their sprite. We can also do some fun stuff like
    creating the following instances:
        Example(depth=10) creates an instance with depth of 10
        Example(x=5, y=10, depth=-100) creates an instance at x=5, y=10, and
                                       with a depth of -100
        Example(5, 10) creates an object at x = 5, y = 10
    """
    
    def __init__(self, *args, **kwargs):
        Slice.__init__(self, image="bogos.gif", *args, **kwargs)
        self.text = "Some text to draw"
    
    def onDraw(self):
        myfont = pygame.font.SysFont("Courier", 15)
        label = myfont.render(self.text, 1, (0, 0, 0))
        window.blit(label, (20, 20))
        window.blit(self.image, (self.x, self.y))
    
    def onStep(self):
        keys = pygame.key.get_pressed()
        
        if keys[K_UP]:
            self.y -= 5
        if keys[K_DOWN]:
            self.y += 5
        if keys[K_RIGHT]:
            self.x += 5
        if keys[K_LEFT]:
            self.x -= 5
        
        if keys[K_DELETE]:
            self.instanceDestroy()
        if keys[K_ESCAPE]:
            pygame.quit()
            sys.exit()


if __name__ == '__main__':
    pygame.init()
    pygame.mixer.music.load("Into_the_Cold.ogg")  # Play some music!
    pygame.mixer.music.play()
    Example(50, 100)
    
    while True:
        """The main game loop. Usually best to not change much in here."""
        pygame.time.Clock().tick(SLEEP_TIME)
        
        window.fill((255, 255, 255))  # TODO: Make this more efficient
        for obj in Slice.lst:
            obj.onBeginStep()
            obj.onStep()
            obj.onPreDraw()
            obj.onDraw()
            obj.onPostDraw()
            obj.onEndStep()
        
        pygame.display.update()
        pygame.display.flip()
        
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
